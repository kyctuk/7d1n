# Cсылки на ресурсы


Shared Folder: https://drive.google.com/drive/folders/15XLjPv-7yd3SSMP4N7ly2kkY89jexdBP?usp=sharing

YouTube: https://www.youtube.com/channel/UCh6uPuvfAorgsdyBo1DgVmA

Discord: https://discord.gg/j55EwdugFK

VK: https://vk.com/open7d1n


# 7d1n

[Вернуться на главную](README.md)

Дата: **2022.01.04**

Текущая версия: **1.0.0.0**

Репозиторий включает исходный код проекта **7d1n** и его ресурсы в полном объёме.

## Лицензия

Этот проект лицензирован по лицензии **MIT License** - подробности смотрите в файле [LICENSE](LICENSE).
