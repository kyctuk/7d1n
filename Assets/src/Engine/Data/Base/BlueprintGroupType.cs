namespace Engine.Data
{

    public enum BlueprintGroupType
    {
        Survive,
        Weapons,
        Tools,
    };

}