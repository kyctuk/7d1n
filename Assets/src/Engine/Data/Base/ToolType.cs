﻿
namespace Engine.Data
{

    /// <summary>
    ///
    /// Тип инструмента
    /// ---
    /// Type of tool
    /// 
    /// </summary>
    public enum ToolType : int
    {
        /// <summary>
        ///     Плоская отвёртка
        ///     ---
        ///     Flathead screwdriver
        /// </summary>
        FlatheadScrewdriver,
        
        /// <summary>
        ///     Крестовая отвёртка
        ///     ---
        ///     Phillips screwdriver
        /// </summary>
        PhillipsScrewdriver,
        
        /// <summary>
        ///     Молоток
        ///     ---
        ///     Hammer
        /// </summary>
        Hammer,
        
        /// <summary>
        ///     Ножовка по дереву
        ///     ---
        ///     Saw
        /// </summary>
        Saw,
        
        /// <summary>
        ///     Ножовка по металлу
        ///     ---
        ///     
        /// </summary>
        Hacksaw,
        
        /// <summary>
        ///     Линейка-угол
        ///     ---
        ///     Corner ruler
        /// </summary>
        Line,
        
        /// <summary>
        ///     Лопата
        ///     ---
        ///     Shovel
        /// </summary>
        Shovel,
        
        /// <summary>
        ///     Кусачки
        ///     ---
        ///     Clippers
        /// </summary>
        Clippers,
        
        /// <summary>
        ///     Гаечный ключ типа А
        ///     ---
        ///     Type A wrench
        /// </summary>
        WrenchTypeA,
        
        /// <summary>
        ///     Гаечный ключ типа Б
        ///     ---
        ///     Type B wrench
        /// </summary>
        WrenchTypeB,
        
        /// <summary>
        ///     Гаечный ключ типа В
        ///     ---
        ///     Type C wrench
        /// </summary>
        WrenchTypeC,
        
        /// <summary>
        ///     Плоскогубцы
        ///     ---
        ///     Pliers
        /// </summary>
        Pliers,
        
        /// <summary>
        ///     Кирка
        ///     ---
        ///     Pickaxe
        /// </summary>
        Pickaxe,

        /// <summary>
        ///     Топор
        ///     ---
        ///     Axe
        /// </summary>
        Axe,
        
        /// <summary>
        ///     Нож
        ///     ---
        ///     Knife
        /// </summary>
        Knife,
        
        /// <summary>
        ///     Лом/Фомка
        ///     ---
        ///     Crowbar
        /// </summary>
        Crowbar,
        
        /// <summary>
        ///     Кувалда
        ///     ---
        ///     Sledgehammer
        /// </summary>
        Sledgehammer,
    }

}
