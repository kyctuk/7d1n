// AUTOGENERATION - Не редактировать!

namespace Engine.Data
{

    /// <summary>
    /// Словарь объектов
    /// </summary>
    public partial class DataDictionary
    {

		/// <summary>
		/// Интерьер
		/// </summary>
        public static class Interiors
        {
			public const long SINK_1 = 100000L;
			public const long COUNTER_CORNER_1 = 100001L;
			public const long COUNTER_EMPTY_1 = 100002L;
			public const long COUNTER_SHELF_1 = 100003L;
			public const long COUNTER_WASHING_1 = 100004L;
			public const long FRIDGE_1 = 100005L;
			public const long OVEN_1 = 100006L;
			public const long KITCHEN_TABLE_1 = 100007L;
			public const long KITCHEN_CHAIR_1 = 100008L;
			public const long HALL_CHAIR_1 = 100100L;
			public const long HALL_COUCH_1 = 100101L;
			public const long HALL_TV_CABINET_1 = 100102L;
			public const long TVSET_1 = 100103L;
			public const long BATH_SINK_1 = 100200L;
			public const long BATH_WASHER_1 = 100201L;
			public const long BATH_1 = 100202L;
			public const long DOOR_1 = 101000L;
		}

	}

}
