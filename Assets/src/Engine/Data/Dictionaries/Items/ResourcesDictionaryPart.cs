// AUTOGENERATION - Не редактировать!

namespace Engine.Data
{

    /// <summary>
    /// Словарь объектов
    /// </summary>
    public partial class DataDictionary
    {

		/// <summary>
		/// Ресурсы
		/// </summary>
        public static class Resources
        {
			public const long RES_WOOD = 0L;
			public const long RES_SCRAP = 1L;
			public const long RES_IRON = 2L;
			public const long RES_LEAD = 3L;
			public const long RES_COOPER = 4L;
			public const long RES_ALUMINUM = 5L;
			public const long RES_SILVER = 6L;
			public const long RES_GOLD = 7L;
			public const long RES_TITANIUM = 8L;
			public const long RES_PLASTIC = 9L;
			public const long RES_PAPER = 10L;
			public const long RES_ELECTRIC = 11L;
			public const long RES_STONE = 12L;
			public const long RES_CERAMICS = 13L;
			public const long RES_SAND = 14L;
			public const long RES_SALT = 15L;
			public const long RES_COAL = 16L;
			public const long RES_WATER = 17L;
			public const long RES_ACID = 18L;
			public const long RES_LYE = 19L;
			public const long RES_BRIMSTONE = 20L;
			public const long RES_SALTPETRE = 21L;
			public const long RES_FUEL = 22L;
			public const long RES_FIBER = 23L;
			public const long RES_RUBBER = 24L;
			public const long RES_POWDER = 25L;
			public const long RES_EXPLOSIVE = 26L;
			public const long RES_DIRTY_WATER = 27L;
			public const long RES_RAW_IRON = 28L;
			public const long RES_RAW_LEAD = 29L;
			public const long RES_RAW_COOPER = 30L;
			public const long RES_RAW_ALUMINUM = 31L;
			public const long RES_RAW_SILVER = 32L;
			public const long RES_RAW_GOLD = 33L;
			public const long RES_RAW_TITANIUM = 34L;
		}

	}

}
