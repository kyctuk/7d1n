﻿
namespace Engine.Logic.Locations.Generator.Environment.Building.Rooms
{

    /// <summary>
    /// 
    /// Объекты для жилого помещения - Зал
    /// ---
    /// Objects for living space - Hall
    /// 
    /// </summary>
    public enum HallItemType : int
    {

        /// <summary>
        ///     Стол под телевизор
        ///     ---
        ///     
        /// </summary>
        TVCabinet,
        
        /// <summary>
        ///     Диван
        ///     ---
        /// 
        /// </summary>
        Couch,
        
        /// <summary>
        ///     Кресло
        ///     ---
        /// 
        /// </summary>
        Chair,
        
        /// <summary>
        ///     Телевизор
        ///     ---
        ///     TV-Set
        /// </summary>
        TVSet,

    }

}
