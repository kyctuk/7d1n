namespace Engine.Logic.Locations.Generator.Environment.Building.Arrangement
{
    
    public enum SurfacePositionAlighmentType
    {
        
        /// <summary>
        ///     По x - цетровать, по y - центровать
        ///     ---
        ///     
        /// </summary>
        CenterCenter,
        
        /// <summary>
        ///     По x - слева, по y - центровать
        ///     ---
        ///     
        /// </summary>
        LeftCenter,
                
        /// <summary>
        ///     По x - справа, по y - центровать
        ///     ---
        ///     
        /// </summary>
        RightCenter,
        
        /// <summary>
        ///     По x - центровать, по y - сверху
        ///     ---
        ///     
        /// </summary>
        CenterTop,
        
        /// <summary>
        ///     По x - центровать, по y - снизу
        ///     ---
        ///     
        /// </summary>
        CenterBottom,
        
    }
    
}