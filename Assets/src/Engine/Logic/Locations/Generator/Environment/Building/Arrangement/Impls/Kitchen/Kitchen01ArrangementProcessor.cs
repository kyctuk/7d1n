﻿using Engine.Logic.Locations.Generator.Environment.Building.Rooms;

namespace Engine.Logic.Locations.Generator.Environment.Building.Arrangement.Impls.Kitchen
{

    public class Kitchen01ArrangementProcessor : ArrangementProcessorBase<KitchenItemType>
    {
        
        #region Properties
        
        public override RoomKindType RoomType => RoomKindType.Kitchen;
        
        #endregion

    }

}
