﻿using Engine.Logic.Locations.Generator.Environment.Building.Rooms;

namespace Engine.Logic.Locations.Generator.Environment.Building.Arrangement.Impls.Kitchen
{

    public class Hall01ArrangementProcessor : ArrangementProcessorBase<HallItemType>
    {
        
        #region Properties
        
        public override RoomKindType RoomType => RoomKindType.Hall;
        
        #endregion

    }

}
