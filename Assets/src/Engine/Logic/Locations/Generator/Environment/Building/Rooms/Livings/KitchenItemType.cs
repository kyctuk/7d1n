﻿
namespace Engine.Logic.Locations.Generator.Environment.Building.Rooms
{

    /// <summary>
    /// 
    /// Объекты для жилого помещения - Кухня
    /// ---
    /// Objects for living space - Kitchen
    /// 
    /// </summary>
    public enum KitchenItemType : int
    {

        /// <summary>
        ///     Раковина
        ///     ---
        ///     Sink
        /// </summary>
        Sink,
        
        /// <summary>
        ///     Ящик с полками (на полу)
        ///     ---
        ///     
        /// </summary>
        Counter,
        
        /// <summary>
        ///     Посудомоечная машинка
        ///     ---
        ///     
        /// </summary>
        CounterWashing,
        
        /// <summary>
        ///     Холодильник
        ///     ---
        ///     
        /// </summary>
        Fridge,
        
        /// <summary>
        ///     Газовая плита
        ///     ---
        ///     
        /// </summary>
        Oven,
        
        /// <summary>
        ///     Столешница
        ///     ---
        /// 
        /// </summary>
        EmptyCounter,
        
        /// <summary>
        ///     Угловая часть кухни
        ///     ---
        /// 
        /// </summary>
        CounterCorner,
        
        /// <summary>
        ///     Кухонный стол
        ///     ---
        ///     
        /// </summary>
        Table,
        
        /// <summary>
        ///     Стул
        ///     ---
        ///     
        /// </summary>
        Chair,
        
        /// <summary>
        ///     Микроволновка
        ///     ---
        ///     
        /// </summary>
        Microwave,
        
        /// <summary>
        ///     Тостер
        ///     ---
        ///     
        /// </summary>
        Toaster,
        
        /// <summary>
        ///     Навесной ящик
        ///     ---
        ///     
        /// </summary>
        Cabinet,
        
        /// <summary>
        ///     Вытяжка над плитой
        ///     ---
        ///     
        /// </summary>
        Extractor,
        
    }

}
