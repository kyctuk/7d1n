using System.Collections.Generic;

namespace Engine.Logic.Locations.Generator
{
    
    public class BuildEnemyInfo
    {

        public List<EnemyPointInfo> EnemyStartPoints { get; set; } = new List<EnemyPointInfo>();

    }
    
}
