# Resource links

Shared Folder: https://drive.google.com/drive/folders/15XLjPv-7yd3SSMP4N7ly2kkY89jexdBP?usp=sharing

YouTube: https://www.youtube.com/channel/UCh6uPuvfAorgsdyBo1DgVmA

Discord: https://discord.gg/j55EwdugFK

VK: https://vk.com/open7d1n


# 7d1n

[Return to main page](README.md)

Date: **2022.01.04**

Current version: **1.0.0.0**

The repository includes the source code of the **7d1n** project and its resources in their entirety.

## License

This project is licensed under the **MIT License** - see the [LICENSE](LICENSE) file for details.
